import 'dart:convert';

import 'package:carnet_digital_mascotas/recursos/variables.dart';
import 'package:http/http.dart' as http;

Future<http.Response> negocios(String latitud, String longitud) async {
  try {
    Map jsonData = {'latitud': latitud, 'logitud': longitud, 'limit': 100};

    final response = await http.post(
        //este tipo de validacion ternaria es necesaria ya que
        //puedes cambiar a un servidor test
        PRODUCTION == 1
            ? Uri.https("$URL_BASE", "/api/negocio/negocios-cercanos")
            : Uri.http("$URL_BASE", "/api/negocio/negocios-cercanos"),
        //  "$ip$puerto/api/login",
        headers: {
          "content-type": "application/json",
        },
        body: json.encode(jsonData));

    return response;
  } catch (e) {
    print(e.toString());
    print("error api");
  }
}
