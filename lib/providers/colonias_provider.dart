import 'dart:convert';

import 'package:carnet_digital_mascotas/models/colonias_model.dart';
import 'package:carnet_digital_mascotas/models/localidades_model.dart';
import 'package:carnet_digital_mascotas/models/municipio_model.dart';
import 'package:carnet_digital_mascotas/recursos/variables.dart';
import 'package:http/http.dart' as http;

Future<List<Colonia>> getColonias(int id) async {
  print(id.toString());
  List<Colonia> listC = [];
  Map jsonData = {
    'codigo_postal': id,
  };
  try {
    final response = await http.post(
      PRODUCTION == 1
          ? Uri.https("$URL_BASE", "/api/auth/get-colonias-por-codigo-postal")
          : Uri.http("$URL_BASE", "/api/auth/get-colonias-por-codigo-postal"),
      // "$ip$puerto/api/get-comunicados",
      headers: {
        "content-type": "application/json",
      },
      body: json.encode(jsonData),
    );
    var a = json.decode(response.body);
    print(a);
    return coloniasFromJson(json.encode(a));
  } catch (e) {
    //print(" aqui esta el error $e");
    return listC;
  }
}

Future<http.Response> getLocalidades(int id) async {
  Map jsonData = {
    'codigo_postal': id,
  };
  try {
    final response = await http.post(
      PRODUCTION == 1
          ? Uri.https("$URL_BASE", "/api/auth/get-datos-codigo-postal")
          : Uri.http("$URL_BASE", "/api/auth/get-datos-codigo-postal"),
      // "$ip$puerto/api/get-comunicados",
      headers: {
        "content-type": "application/json",
      },
      body: json.encode(jsonData),
    );

    return response;
  } catch (e) {
    print(" aqui esta el error $e");
    return null;
  }
}

Future<List<Municipio>> getMunicipios(int id) async {
  List<Municipio> listC = [];
  Map jsonData = {
    'codigo_postal': id,
  };
  try {
    final response = await http.post(
      PRODUCTION == 1
          ? Uri.https("$URL_BASE", "/api/auth/get-datos-codigo-postal")
          : Uri.http("$URL_BASE", "/api/auth/get-datos-codigo-postal"),
      // "$ip$puerto/api/get-comunicados",
      headers: {
        "content-type": "application/json",
      },
      body: json.encode(jsonData),
    );
    var a = json.decode(response.body);
    print(a);
    return municipiosFromJson(json.encode(a["data"]["municipios"]));
  } catch (e) {
    print(" aqui esta el error $e");
    return listC;
  }
}
