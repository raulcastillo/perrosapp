import 'dart:convert';

import 'package:carnet_digital_mascotas/recursos/variables.dart';
import 'package:http/http.dart' as http;

Future<http.Response> login(String email, String password) async {
  try {
    Map jsonData = {'email': email, 'password': password};

    final response = await http.post(
        //este tipo de validacion ternaria es necesaria ya que
        //puedes cambiar a un servidor test
        PRODUCTION == 1
            ? Uri.https("$URL_BASE", "/api/auth/login")
            : Uri.http("$URL_BASE", "/api/auth/login"),
        //  "$ip$puerto/api/login",
        headers: {
          "content-type": "application/json",
        },
        body: json.encode(jsonData));

    return response;
  } catch (e) {
    print(e.toString());
    print("error api");
  }
}

Future<http.Response> registrar(
  nombre,
  primerApellido,
  segundoApellido,
  edad,
  sexo,
  password,
  username,
  telefono,
  email,
  calle,
  interior,
  coloniaId,
  localidadId,
  municipioId,
  exterior,
) async {
  try {
    Map jsonData = {
      'nombre': nombre,
      'primer_apellido': primerApellido,
      'segundo_apellido': segundoApellido,
      'edad': edad,
      'sexo': sexo,
      'password': password,
      'username': username,
      'telefono': telefono,
      'email': email,
      'calle': calle,
      'interior': interior,
      'colonia_id': coloniaId,
      'localidad_id': localidadId,
      'municipio_id': municipioId,
      'exterior': exterior
    };
    print(jsonData);

    final response = await http.post(
        //este tipo de validacion ternaria es necesaria ya que
        //puedes cambiar a un servidor test
        PRODUCTION == 1
            ? Uri.https("$URL_BASE", "/api/auth/registro-usuario")
            : Uri.http("$URL_BASE", "/api/auth/registro-usuario"),
        //  "$ip$puerto/api/login",
        headers: {
          "content-type": "application/json",
        },
        body: json.encode(jsonData));

    return response;
  } catch (e) {
    print(e);
    return null;
  }
}
