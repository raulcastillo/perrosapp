import 'package:carnet_digital_mascotas/page/mascotas/gatos.dart';
import 'package:carnet_digital_mascotas/page/mascotas/otros.dart';
import 'package:carnet_digital_mascotas/page/mascotas/perros.dart';
import 'package:carnet_digital_mascotas/widgets/buscador.dart';
import 'package:carnet_digital_mascotas/widgets/menu.dart';
import 'package:drawer_swipe/drawer_swipe.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MascotasPerdidasPage extends StatefulWidget {
  @override
  _MascotasPerdidasPageState createState() => _MascotasPerdidasPageState();
}

class _MascotasPerdidasPageState extends State<MascotasPerdidasPage> {
  bool banderaTabsTodos = true;
  bool banderaTabsPerros = false;
  bool banderaTabsGatos = false;
  bool banderaTabsOtros = false;
  var drawerKey3 = GlobalKey<SwipeDrawerState>();
  TabController tabcontroler;
  final List<Tab> mascotasTabs = <Tab>[
    Tab(
      child: Text('Perros'),
    ),
    Tab(
      child: Text('Gatos'),
    ),
    Tab(
      child: Text('Otros'),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        body: SwipeDrawer(
          radius: 20,
          key: drawerKey3,
          hasClone: false,
          bodyBackgroundPeekSize: 30,
          backgroundColor: Colors.lightBlue,
          drawer: MenuWidget(),
          child: buildBody(size),
        ),
      ),
    );
  }

  Widget buildBody(final size) {
    return Expanded(
      child: Column(
        children: [
          AppBar(
            title: Center(
                child: Text(
              'Mascotas perdidas',
              style: TextStyle(color: Colors.black),
            )),
            backgroundColor: Colors.white,
            leading: InkWell(
                onTap: () {
                  if (drawerKey3.currentState.isOpened()) {
                    drawerKey3.currentState.closeDrawer();
                  } else {
                    drawerKey3.currentState.openDrawer();
                  }
                },
                child: Icon(
                  Icons.menu,
                  color: Colors.black,
                )),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            actions: [
              IconButton(
                icon: Icon(
                  Icons.person_outline_rounded,
                  color: Colors.black,
                ),
                onPressed: () {
                  // do something
                },
              )
            ],
          ),
          // SizedBox(
          //   height: 30,
          // ),

          buscarLavel(size),
          Container(
            padding: EdgeInsets.only(right: 10, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      print(banderaTabsTodos);
                      setState(() {
                        banderaTabsTodos = true;
                        banderaTabsPerros = false;
                        banderaTabsGatos = false;
                        banderaTabsOtros = false;
                      });
                    },
                    child: banderaTabsTodos != false
                        ? Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.blue,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Todos',
                                style: TextStyle(color: Colors.white),
                              ),
                            ))
                        : Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Todos',
                                style: TextStyle(color: Colors.black),
                              ),
                            ))),
                GestureDetector(
                    onTap: () {
                      setState(() {
                        banderaTabsTodos = false;
                        banderaTabsPerros = true;
                        banderaTabsGatos = false;
                        banderaTabsOtros = false;
                      });
                    },
                    child: banderaTabsPerros != false
                        ? Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.blue,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Perros',
                                style: TextStyle(color: Colors.white),
                              ),
                            ))
                        : Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Perros',
                                style: TextStyle(color: Colors.black),
                              ),
                            ))),
                GestureDetector(
                    onTap: () {
                      setState(() {
                        banderaTabsTodos = false;
                        banderaTabsPerros = false;
                        banderaTabsGatos = true;
                        banderaTabsOtros = false;
                      });
                    },
                    child: banderaTabsGatos != false
                        ? Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.blue,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Gatos',
                                style: TextStyle(color: Colors.white),
                              ),
                            ))
                        : Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Gatos',
                                style: TextStyle(color: Colors.black),
                              ),
                            ))),
                GestureDetector(
                    onTap: () {
                      setState(() {
                        banderaTabsTodos = false;
                        banderaTabsPerros = false;
                        banderaTabsGatos = false;
                        banderaTabsOtros = true;
                      });
                    },
                    child: banderaTabsOtros != false
                        ? Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.blue,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Otros',
                                style: TextStyle(color: Colors.white),
                              ),
                            ))
                        : Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            // padding: EdgeInsets.all(5),
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Text(
                                'Otros',
                                style: TextStyle(color: Colors.black),
                              ),
                            ))),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: makeFeed(
                        userName: 'Aiony Haust',
                        userImage: 'assets/chaconImagen.jpeg',
                        feedTime: '1 hr ago',
                        feedText:
                            'All the Lorem Ipsum generators on the Internet tend to repeat predefined.',
                        feedImage: 'assets/chaconImagen.jpeg'),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: makeFeed(
                        userName: 'Aiony Haust',
                        userImage: 'assets/chaconImagen.jpeg',
                        feedTime: '1 hr ago',
                        feedText:
                            'All the Lorem Ipsum generators on the Internet tend to repeat predefined.',
                        feedImage: 'assets/chaconImagen.jpeg'),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: makeFeed(
                        userName: 'Aiony Haust',
                        userImage: 'assets/chaconImagen.jpeg',
                        feedTime: '1 hr ago',
                        feedText:
                            'All the Lorem Ipsum generators on the Internet tend to repeat predefined.',
                        feedImage: 'assets/chaconImagen.jpeg'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  String capitalize(String input) {
    if (input == null) {
      throw new ArgumentError("string: $input");
    }
    if (input.length == 0) {
      return input;
    }
    return input[0].toUpperCase() + input.substring(1);
  }

  Widget buscarLavel(size) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Search(
        hintText: "Buscar mascota",
        onChanged: (val) {
          //busquedaNoticias = lista;
          if (this.mounted) {
            setState(() {
              List busquedaNoticias = [];
              busquedaNoticias
                  .where((element) =>
                      element.nombre.contains(capitalize(val)) ||
                      element.comunicado_por.contains(capitalize(val)) ||
                      element.nombre
                          .toUpperCase()
                          .contains(capitalize(val.toUpperCase())) ||
                      element.comunicado_por
                          .toUpperCase()
                          .contains(capitalize(val.toUpperCase())) ||
                      element.nombre
                          .toLowerCase()
                          .contains(capitalize(val.toLowerCase())) ||
                      element.comunicado_por
                          .toLowerCase()
                          .contains(capitalize(val.toLowerCase())))
                  .toList();
              if (this.mounted) {
                //   lista.isEmpty &&
                setState(() {});
              }
            });
          }
        },
      ),
    );
  }

  Widget makeFeed({userName, userImage, feedTime, feedText, feedImage}) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage(userImage), fit: BoxFit.cover)),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        userName,
                        style: TextStyle(
                            color: Colors.grey[900],
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        feedTime,
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                    ],
                  )
                ],
              ),
              IconButton(
                icon: Icon(
                  Icons.more_horiz,
                  size: 30,
                  color: Colors.grey[600],
                ),
                onPressed: () {},
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            feedText,
            style: TextStyle(
                fontSize: 15,
                color: Colors.grey[800],
                height: 1.5,
                letterSpacing: .7),
          ),
          SizedBox(
            height: 20,
          ),
          feedImage != ''
              ? Container(
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage(feedImage), fit: BoxFit.cover)),
                )
              : Container(),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  makeLike(),
                  Transform.translate(offset: Offset(-5, 0), child: makeLove()),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "2.5K",
                    style: TextStyle(fontSize: 15, color: Colors.grey[800]),
                  )
                ],
              ),
              Text(
                "400 Comments",
                style: TextStyle(fontSize: 13, color: Colors.grey[800]),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              makeLikeButton(isActive: true),
              makeCommentButton(),
              makeShareButton(),
            ],
          )
        ],
      ),
    );
  }

  Widget makeLike() {
    return Container(
      width: 25,
      height: 25,
      decoration: BoxDecoration(
          color: Colors.blue,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)),
      child: Center(
        child: Icon(Icons.thumb_up, size: 12, color: Colors.white),
      ),
    );
  }

  Widget makeLove() {
    return Container(
      width: 25,
      height: 25,
      decoration: BoxDecoration(
          color: Colors.red,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)),
      child: Center(
        child: Icon(Icons.favorite, size: 12, color: Colors.white),
      ),
    );
  }

  Widget makeLikeButton({isActive}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey[200]),
        borderRadius: BorderRadius.circular(50),
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.thumb_up,
              color: isActive ? Colors.blue : Colors.grey,
              size: 18,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              "Like",
              style: TextStyle(color: isActive ? Colors.blue : Colors.grey),
            )
          ],
        ),
      ),
    );
  }

  Widget makeCommentButton() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey[200]),
        borderRadius: BorderRadius.circular(50),
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.chat, color: Colors.grey, size: 18),
            SizedBox(
              width: 5,
            ),
            Text(
              "Comment",
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }

  Widget makeShareButton() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey[200]),
        borderRadius: BorderRadius.circular(50),
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.share, color: Colors.grey, size: 18),
            SizedBox(
              width: 5,
            ),
            Text(
              "Share",
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }
}
