import 'package:carnet_digital_mascotas/widgets/menu.dart';
import 'package:drawer_swipe/drawer_swipe.dart';
import 'package:flutter/material.dart';

class DetalleMascotasPerdidas extends StatefulWidget {
  @override
  _DetalleMascotasPerdidasState createState() =>
      _DetalleMascotasPerdidasState();
}

class _DetalleMascotasPerdidasState extends State<DetalleMascotasPerdidas> {
  var drawerKey4 = GlobalKey<SwipeDrawerState>();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: SwipeDrawer(
        radius: 20,
        key: drawerKey4,
        hasClone: false,
        bodyBackgroundPeekSize: 30,
        backgroundColor: Colors.lightBlue,
        drawer: MenuWidget(),
        child: buildBody(size),
      ),
    );
  }
}

Widget buildBody(final size) {}

//bottom sheet donde ira la demas informacion de la mascota que se encuentra perdida
void displayBottomSheet(BuildContext context) {
  showModalBottomSheet(
      context: context,
      builder: (ctx) {
        return Container(
          //  height: MediaQuery.of(context).size.height * 0.6,
          child: Column(
            children: [
              Center(
                  child: Container(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(150.0, 5.0, 150.0, 0.0),
                  child: Divider(
                    color: Colors.black,
                    thickness: 4,
                    height: 30,
                  ),
                ),
              )),
            ],
          ),
        );
      });
}
