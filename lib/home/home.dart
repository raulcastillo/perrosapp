import 'package:carnet_digital_mascotas/widgets/appbarBalchi.dart';
import 'package:carnet_digital_mascotas/widgets/menu.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:drawer_swipe/drawer_swipe.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var drawerKey = GlobalKey<SwipeDrawerState>();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: SwipeDrawer(
        radius: 20,
        key: drawerKey,

        hasClone: false,
        bodyBackgroundPeekSize: 30,
        backgroundColor: Colors.lightBlue,
        // pass drawer widget
        drawer: MenuWidget(),
        // pass body widget
        child: buildBody(size),
      ),
    );
  }

  Widget cardPrueba() {
    return Card(
      // Con esta propiedad modificamos la forma de nuestro card
      // Aqui utilizo RoundedRectangleBorder para proporcionarle esquinas circulares al Card
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),

      // Con esta propiedad agregamos margen a nuestro Card
      // El margen es la separación entre widgets o entre los bordes del widget padre e hijo
      margin: EdgeInsets.all(15),

      // Con esta propiedad agregamos elevación a nuestro card
      // La sombra que tiene el Card aumentará
      elevation: 10,

      // La propiedad child anida un widget en su interior
      // Usamos columna para ordenar un ListTile y una fila con botones
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            // Usamos ListTile para ordenar la información del card como titulo, subtitulo e icono
            ListTile(
              contentPadding: EdgeInsets.fromLTRB(15, 10, 25, 0),
              title: Text("item.titulo"),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("item.contenido"),
                ],
              ),
              leading: ClipRRect(
                child: Container(
                  height: 100,
                  width: 100,
                  child: Image(
                    image: NetworkImage(
                        "https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_3c800000-03a40002.png"),
                  ),
                ),
              ),
            ),
            //SizedBox(height: 20,),
            // Usamos una fila para ordenar los botones del card
          ],
        ),
      ),
    );
  }

  Widget buildBody(final size) {
    return SingleChildScrollView(
      child: Column(
        children: [
          //appbar con funciones para abrir el menu
          AppbarBalchi2(
            label: "Balchi",
            drawerKey2: drawerKey,
          ),
          // build your screen body
          Column(
            children: [
              SizedBox(
                height: 20,
              ),
              CarouselSlider(
                options: CarouselOptions(
                    height: 200.0, autoPlay: true, pageSnapping: true),
                items: [
                  "https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_00000000-00000002.png",
                  "https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_01000000-00040002.png",
                  "https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_01010000-000e0002.png",
                  "https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_3a000000-03a10002.png",
                ].map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 5.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: Colors.black)),
                        child: Image(
                          image: NetworkImage("$i"),
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
              SizedBox(
                height: 45,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Text(
                    "Noticias",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 21),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: size.height * .54,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      cardPrueba(),
                      cardPrueba(),
                      cardPrueba(),
                      cardPrueba(),
                      cardPrueba(),
                      cardPrueba(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
