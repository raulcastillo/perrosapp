import 'dart:convert';

Colonia coloniaFromJson(String str) => Colonia.fromJson(json.decode(str));

String coloniaToJson(Colonia data) => json.encode(data.toJson());

class Colonia {
  int id;
  String descripcion;
  String codigoPostal;
  int localidadId;
  int active;
  Colonia({
    this.id,
    this.descripcion,
    this.codigoPostal,
    this.localidadId,
    this.active,
  });

  factory Colonia.fromJson(Map<String, dynamic> json) {
    //(Persona);
    return Colonia(
      id: json["id"],
      descripcion: json["descripcion"],
      codigoPostal: json["codigo_postal"],
      localidadId: json["localidad_id"],
      active: json["active"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "descripcion": descripcion,
      "codigo_postal": codigoPostal,
      "localidad_id": localidadId,
      "active": active,
    };
  }
}

List<Colonia> coloniasFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<Colonia>.from(str.map((item) {
    return Colonia.fromJson(item);
  }));
}

String coloniasToJson(Colonia data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
