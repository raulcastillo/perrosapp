import 'dart:convert';

Localidad localidadFromJson(String str) => Localidad.fromJson(json.decode(str));

String localidadToJson(Localidad data) => json.encode(data.toJson());

class Localidad {
  int id;
  String descripcion;
  int municipioId;
  int localidadId;
  int active;
  Localidad({
    this.id,
    this.descripcion,
    this.municipioId,
    this.localidadId,
    this.active,
  });

  factory Localidad.fromJson(Map<String, dynamic> json) {
    //(Persona);
    return Localidad(
      id: json["id"],
      descripcion: json["descripcion"],
      municipioId: json["municipio_id"],
      localidadId: json["localidad_id"],
      active: json["active"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "descripcion": descripcion,
      "municipio_id": municipioId,
      "localidad_id": localidadId,
      "active": active,
    };
  }
}

List<Localidad> localidadesFromJson(String strJson) {
  final str = json.decode(strJson);
  ////(str);
  return List<Localidad>.from(str.map((item) {
    ////(item);
    return Localidad.fromJson(item);
  }));
}

String localidadesToJson(Localidad data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
