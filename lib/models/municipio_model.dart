import 'dart:convert';

Municipio municipioFromJson(String str) => Municipio.fromJson(json.decode(str));

String municipioToJson(Municipio data) => json.encode(data.toJson());

class Municipio {
  int id;
  String descripcion;
  int entidadFederativaId;
  int active;
  Municipio({
    this.id,
    this.descripcion,
    this.entidadFederativaId,
    this.active,
  });

  factory Municipio.fromJson(Map<String, dynamic> json) {
    //(Persona);
    return Municipio(
      id: json["id"],
      descripcion: json["descripcion"],
      entidadFederativaId: json["entidad_federativa_id"],
      active: json["active"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "descripcion": descripcion,
      "entidad_federativa_id": entidadFederativaId,
      "active": active,
    };
  }
}

List<Municipio> municipiosFromJson(String strJson) {
  final str = json.decode(strJson);
  ////(str);
  return List<Municipio>.from(str.map((item) {
    ////(item);
    return Municipio.fromJson(item);
  }));
}

String municipiosToJson(Municipio data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
