import 'dart:convert';

class Negocios {
  int id;
  String nombre;

  Negocios({
    this.id,
    this.nombre,
  });

  factory Negocios.fromJson(Map<String, dynamic> json) {
    return Negocios(
      id: json["id"],
      nombre: json["nombre"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "nombre": nombre,
    };
  }
}

List<Negocios> negociosFromJson(String strJson) {
  final str = json.decode(strJson);
  ////(str);
  return List<Negocios>.from(str.map((item) {
    ////(item);
    return Negocios.fromJson(item);
  }));
}

String negociosToJson(Negocios data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
