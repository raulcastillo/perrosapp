import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetalleAdopcion extends StatefulWidget {
  DetalleAdopcion({Key key}) : super(key: key);

  @override
  _DetalleAdopcionState createState() => _DetalleAdopcionState();
}

class _DetalleAdopcionState extends State<DetalleAdopcion> {

  /* Declarar variables */
  final estiloTitulo    = TextStyle( fontSize: 30.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 18.0, color: Colors.grey );
  final List listPhotosUrl =[
    "https://media-cdn.tripadvisor.com/media/photo-s/06/be/c8/78/parque-municipal-los.jpg",
    "https://s1.wklcdn.com/image_36/1080019/8696283/5120587Master.jpg",
    "https://mapio.net/images-p/1172789.jpg",
    "https://i.pinimg.com/originals/1a/5e/04/1a5e04bc742a7942375989650091d9c9.jpg",
  ];
  final List listPhotosUr2 =[
    "https://mapio.net/images-p/65452888.jpg"
    "https://mapio.net/images-p/99093562.jpg",
    "https://mapio.net/images-p/101127451.jpg",
    "https://www.vivieloeste.com.ar/wp-content/uploads/2018/04/reserva-los-robles1-e1522927107390.jpg",
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _createAppBarImagen(context: context,url: 'https://4.bp.blogspot.com/_3hx9PRFfaGA/Si12xIv0WwI/AAAAAAAAAR4/DHgtPCizBRc/s400/1172789.jpg'),
            _createTitle(title: "Balchi",description:"La Presa Ingeniero Carlos F. Roggero es una presa reguladora del cauce del Río Reconquista construida en 1972 y que se ubica a 40 km de la ciudad de Buenos Aires, entre La Reja y Mariano Acosta, partidos de Moreno, Merlo, Marcos Paz y General Rodríguez, provincia de Buenos Aires, Argentina"),
            _createActions(),
            _createlistViewCardsHorizontalPhotos(reverse: false,listPhotos: listPhotosUrl,widthCard: 300.0,heightCard: 200.0),
            _createText(reverse: false,text:"La presa contiene las aguas del embalse Lago San Francisco que tiene una extensión de 260 ha; de las cuales 50 que pertenecen al partido de Merlo, otras 120 al de Moreno junto con la represa, unas 50 al partido de General Rodríguez y las 30 restantes al partido de Marcos Paz."),
            _createlistViewCardsHorizontalPhotos(reverse: true,listPhotos: listPhotosUr2,widthCard: 300.0,heightCard: 100.0),
            //_createText(reverse: true,text:"El parque municipal es una de las 11 áreas protegidas dentro del AMBA de las que se puede mencionar a la reserva natural Otamendi y a la reserva ecológica Costanera Sur.Un 40 % de la superficie del parque municipal está ocupada por bosques, otro 40 % por lagos y lagunas, un 10 % por matorrales, el resto por arroyos y pastizales."),
          ],
        ),
      ),
    );
  }

  /* WIDGETS VIES */
  Widget _createlistViewCardsHorizontalPhotos({@required List listPhotos,bool reverse=false,double circleBoderCard=20.0,double widthCard=150,double heightCard=150}){
    return Container(
      width:double.infinity,height: heightCard+30,
      child: ListView.builder(
        padding: EdgeInsets.only(left:reverse?0:40,right:reverse?40:0),
        reverse: reverse,
        shrinkWrap: true,
        scrollDirection:Axis.horizontal,
        itemCount:listPhotos.length,
        itemBuilder: (context, index) => cardPhotoMovie(url: listPhotos[index],height: heightCard,width: widthCard,circleBoder: circleBoderCard),
      ),
    );
  }
  Widget _createAppBarImagen({@required BuildContext context,@required String url,double height=300.0}) {
    return Stack(
      children: [
        Container(width: double.infinity,height:height,child: Image(image: NetworkImage(url),fit: BoxFit.cover)),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 50.0,horizontal: 12.0),
          child: FloatingActionButton(onPressed: (){Navigator.of(context).pop();},child:  Icon(Icons.arrow_back,color: Theme.of(context).brightness==Brightness.dark?Colors.white:Colors.black ),elevation: 0.0,backgroundColor: Theme.of(context).brightness==Brightness.dark?Colors.black:Colors.white ),
        ),
      ],
    );
  }

  Widget _createTitle({@required String title,String description=""}) {

    return Container(
      
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
  
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(title, style: estiloTitulo ),
              ),
                          clip(text: "Adopcion",backgroundColor: Colors.amber.withOpacity(0.1),colorIcon:Colors.amber,colorText: Colors.amber,iconData: Icons.star,iconVisibility: true),
            ],
          ),
          Text(description, style: estiloSubTitulo ,textAlign:TextAlign.center),
        ],
      ),
    );
  }
  Widget _createActions() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _createButtonAcctions( Icons.call, 'Telefono'),
          _createButtonAcctions( Icons.near_me, 'Ubicacion' ),
          _createButtonAcctions( Icons.share, 'Compartir'),
        ],
      ),
    );
  }
  Widget _createButtonAcctions(IconData icon, String texto ) {
    return Column(
      children: <Widget>[
        Icon( icon, color: Colors.red, size: 40.0 ),
        SizedBox( height: 5.0 ),
        Text( texto, style: TextStyle( fontSize: 12.0, color: Colors.blue ), )
      ],
    );

  }

  Widget _createText({bool reverse=false,@required String text}) {

    return Padding(
      padding: EdgeInsets.only(bottom: 12.0,top: 12.0,left: reverse?40.0:8.0,right: reverse?8.0:40.0),
      child: Text(text,textAlign: TextAlign.justify,style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
    );

  }

  /* WIDGETS COMPONENTS */
  Widget clip({double size=24.0,@required String text,IconData iconData=Icons.check_circle,bool iconVisibility=false,Color backgroundColor=Colors.blue,Color colorText=Colors.white,Color colorIcon=Colors.white})=>Chip(backgroundColor: backgroundColor,shape: StadiumBorder(side: BorderSide(color: backgroundColor)),label: Text(text,style: TextStyle(fontWeight:FontWeight.bold,color: colorText)),avatar:iconVisibility?Icon(iconData,color:colorIcon):null,elevation: 0.0);
  Widget cardPhotoMovie({ @required String url,double circleBoder=0.0,double width=150.0,double height=150.0}){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 2.0,vertical:12.0),
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(circleBoder)),
      child: InkWell(
        child: Container(
          width:width,height: height,
          child: CachedNetworkImage(fadeInDuration: Duration(milliseconds: 200),fit: BoxFit.cover,imageUrl:url,placeholder: (context, urlImage) => Container(color: Colors.grey),errorWidget: (context, urlImage, error) => Center(child: Container(color: Colors.grey))),
        ),
      ),
    );
  }
}