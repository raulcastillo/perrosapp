import 'package:carnet_digital_mascotas/mascotas_perdidas/detalles_mascota_perdida.dart';
import 'package:carnet_digital_mascotas/widgets/menu.dart';
import 'package:drawer_swipe/drawer_swipe.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';

class NegociosPage extends StatefulWidget {
  NegociosPage({Key key}) : super(key: key);
  @override
  _NegociosPageState createState() => _NegociosPageState();
}

class _NegociosPageState extends State<NegociosPage> {
  Position _currentPosition;
  String _currentAddress;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCurrentLocation();
  }

  _getCurrentLocation() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best,
            forceAndroidLocationManager: true)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        print(_currentPosition.latitude);
        print(_currentPosition.longitude);
        _getAddressFromLatLng();
      });
    }).catchError((e) {
      print('$e dwdsada');
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = placemarks[0];

      setState(() {
        _currentAddress =
            "${place.locality}, ${place.postalCode}, ${place.country}";
        print(_currentAddress);
      });
    } catch (e) {
      print(e);
    }
  }

  var drawerKey = GlobalKey<SwipeDrawerState>();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: SwipeDrawer(
        radius: 20,
        key: drawerKey,
        hasClone: false,
        bodyBackgroundPeekSize: 30,
        backgroundColor: Colors.lightBlue,
        drawer: MenuWidget(),
        child: buildBodyTwo(size),
      ),
    );
  }

  Widget cardPrueba() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: EdgeInsets.all(15),
      elevation: 10,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.fromLTRB(15, 10, 25, 0),
              title: Text("item.titulo"),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("item.contenido"),
                ],
              ),
              leading: ClipRRect(
                child: Container(
                  height: 100,
                  width: 100,
                  child: Image(
                    image: NetworkImage(
                        "https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_3c800000-03a40002.png"),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBodyTwo(final size) {
    return SingleChildScrollView(
      child: Column(
        children: [
          AppBar(
            title: Center(
              child: Text(
                'Balchi',
                style: TextStyle(color: Colors.black),
              ),
            ),
            backgroundColor: Colors.white,
            leading: InkWell(
              onTap: () {
                if (drawerKey.currentState.isOpened()) {
                  drawerKey.currentState.closeDrawer();
                } else {
                  drawerKey.currentState.openDrawer();
                }
              },
              child: Icon(
                Icons.menu,
                color: Colors.black,
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            actions: [
              IconButton(
                icon: Icon(
                  Icons.person_outline_rounded,
                  color: Colors.black,
                ),
                onPressed: () {},
              )
            ],
          ),
          Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Text("Tu ubicación"),
              SizedBox(
                height: 10,
              ),
              _currentAddress != null
                  ? Text(_currentAddress)
                  : CircularProgressIndicator(),
              Container(
                height: size.height * .54,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      cardPrueba(),
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
