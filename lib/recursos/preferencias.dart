import 'package:carnet_digital_mascotas/introduccion/introduccion_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences prefs;

  initPrefs() async {
    WidgetsFlutterBinding.ensureInitialized();
    this.prefs = await SharedPreferences.getInstance();
  }

  int get id {
    return this.prefs.getInt("id") ?? 0;
  }

  set id(int id) {
    this.prefs?.setInt("id", id);
  }

  int get municipioId {
    return this.prefs.getInt("municipio_id") ?? 0;
  }

  set municipioId(int municipio_id) {
    this.prefs?.setInt("municipio_id", municipio_id);
  }

  int get estructuraOrganizacionalId {
    return this.prefs.getInt("estructura_organizacional_id") ?? 0;
  }

  set estructuraOrganizacionalId(int estructura_organizacional_id) {
    this
        .prefs
        ?.setInt("estructura_organizacional_id", estructura_organizacional_id);
  }

  int get datosGuardados {
    return this.prefs.getInt("datosGuardados") ?? 0;
  }

  set datosGuardados(int datosGuardados) {
    this.prefs.setInt("datosGuardados", datosGuardados);
  }

  String get nombre {
    return this.prefs.getString("nombre") ?? "";
  }

  set nombre(String nombre) {
    this.prefs?.setString("nombre", nombre);
  }

  String get userName {
    return this.prefs.getString("user_name") ?? "";
  }

  set userName(String userName) {
    this.prefs?.setString("user_name", userName);
  }

  String get apellidoPaterno {
    return this.prefs.getString("apellidoPaterno") ?? "";
  }

  set apellidoPaterno(String apellidoPaterno) {
    this.prefs?.setString("apellidoPaterno", apellidoPaterno);
  }

  String get apellidoMaterno {
    return this.prefs.getString("apellidoMaterno") ?? "";
  }

  set apellidoMaterno(String apellidoMaterno) {
    this.prefs?.setString("apellidoMaterno", apellidoMaterno);
  }

  String get rfc {
    return this.prefs.getString("rfc") ?? "";
  }

  set rfc(String rfc) {
    this.prefs.setString("rfc", rfc);
  }

  String get email {
    return this.prefs.getString("email") ?? "";
  }

  set email(String email) {
    this.prefs.setString("email", email);
  }

  String get telefono {
    return this.prefs.getString("telefono") ?? "";
  }

  set telefono(String telefono) {
    this.prefs.setString("telefono", telefono);
  }

  String get token {
    return this.prefs.getString("token") ?? "";
  }

  set token(String token) {
    this.prefs.setString("token", token);
  }

  String get curp {
    return this.prefs.getString("curp") ?? "";
  }

  set curp(String curp) {
    this.prefs.setString("curp", curp);
  }

  String get nombreCompleto {
    return this.prefs.getString("nombreCompleto") ?? "";
  }

  set nombreCompleto(String nombreCompleto) {
    this.prefs.setString("nombre_completo", nombreCompleto);
  }

  int get roleId {
    return this.prefs.getInt("roleId") ?? 0;
  }

  set roleId(int roleId) {
    this.prefs.setInt("roleId", roleId);
  }

  int get personaId {
    return this.prefs.getInt("personaId") ?? 0;
  }

  set personaId(int personaId) {
    this.prefs.setInt("personaId", personaId);
  }

//permisos en proceso
  bool get contactos {
    return this.prefs.getBool('contactos') ?? false;
  }

  set contactos(bool contactos) {
    this.prefs.setBool('contactos', contactos);
  }

  bool get introduccion {
    return this.prefs.getBool('introduccion') ?? false;
  }

  set introduccion(bool introduccion) {
    this.prefs.setBool('introduccion', introduccion);
  }
}
