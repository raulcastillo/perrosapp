import 'package:carnet_digital_mascotas/recursos/preferencias.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

String ip = "192.168.0.10:";
String puerto = "8000";
String puertoSocket = '3000';
String URL_BASE = "$ip$puerto";

//este es el que consume el socket, si estan en un servidor con centificado ssl debes de poner wss, si estas en un servidor no seguro poner
//ws
String URL_BASE_SOCKET = "$ip$puertoSocket";

IO.Socket socket;

//este production se cambia cuando estas en un servidor ssl estos empiezan con https,
//en dado caso de estar en local debe ser 0 ya que es un servidor no seguro osea http.
int PRODUCTION = 0;
PreferenciasUsuario prefs = new PreferenciasUsuario();
