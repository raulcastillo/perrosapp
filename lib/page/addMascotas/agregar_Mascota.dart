import 'package:carnet_digital_mascotas/models/colors.dart';
import 'package:carnet_digital_mascotas/utilities/constants.dart';
import 'package:carnet_digital_mascotas/widgets/lavel_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AgregarMascota extends StatefulWidget {
  @override
  _AgregarMascotaState createState() => _AgregarMascotaState();
}

class _AgregarMascotaState extends State<AgregarMascota> {
  final formKey = GlobalKey<FormState>();
  DateTime selectedDate = DateTime(1900, 1);
  String esterilizado;
  List<String> opcionesEster = ['Si', 'No'];
  TextEditingController nombre = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF73AEF5),
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Stack(
          children: [
            Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFF73AEF5),
                    Color(0xFF61A4F1),
                    Color(0xFF478DE0),
                    Color(0xFF398AE5),
                  ],
                  stops: [0.1, 0.4, 0.7, 0.9],
                ),
              ),
            ),
            Container(
              height: double.infinity,
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(
                  horizontal: 40.0,
                  vertical: 30.0,
                ),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          CircleAvatar(
                            radius: 60,
                            backgroundColor: Colors.white,
                            backgroundImage: AssetImage("assets/mascotas.png"),
                          ),
                          crearBotonCircular()
                        ],
                      ),
                      SizedBox(height: 10.0),
                      LavelText(
                          prefixIcon: Icon(
                            FontAwesomeIcons.paw,
                            color: Colors.white,
                          ),
                          controller: nombre,
                          lavel: "Nombre",
                          hintText: "Nombre"),
                      SizedBox(height: 10.0),
                      _getDatePickerEnabled(),
                      SizedBox(height: 10.0),
                      LavelText(
                          prefixIcon: Icon(
                            FontAwesomeIcons.weight,
                            color: Colors.white,
                          ),
                          controller: nombre,
                          lavel: "Peso",
                          hintText: "Peso"),
                      SizedBox(height: 10.0),
                      LavelText(
                          prefixIcon: Icon(
                            FontAwesomeIcons.weight,
                            color: Colors.white,
                          ),
                          controller: nombre,
                          lavel: "Altura",
                          hintText: "Altura"),
                      SizedBox(height: 10.0),
                      buildEsterelizado(),
                      SizedBox(height: 10.0),
                      genero(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget crearBotonCircular() {
    return Container(
      padding: const EdgeInsets.only(left: 80, top: 80),
      child: ClipOval(
        child: Material(
          color: HexColor("#2F897F"), // button color
          child: InkWell(
            splashColor: Colors.red, // inkwell color
            child: SizedBox(
                width: 46,
                height: 46,
                child: Icon(Icons.camera_alt, color: Colors.white)),
            onTap: () {
              // _modalFoto(context);
            },
          ),
        ),
      ),
    );
  }

  Widget _getDatePickerEnabled() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Fecha de nacimiento',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: Padding(
            padding: EdgeInsets.only(left: 40),
            child: InkWell(
              onTap: () {
                _selectDate(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  selectedDate == DateTime(1900, 1)
                      ? Text(
                          "Seleccione su fecha",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                          //style: CustomTheme.of(context).textTheme.subhead.copyWith(),
                        )
                      : Text(
                          "${selectedDate.day} - ${selectedDate.month} - ${selectedDate.year}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                          //style: CustomTheme.of(context).textTheme.subhead.copyWith(),
                        ),
                  Icon(Icons.arrow_drop_down,
                      color: Theme.of(context).brightness == Brightness.light
                          ? Colors.grey.shade700
                          : Colors.white70),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 1),
        lastDate: DateTime(2021, 12));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        selectedDate =
            DateTime(selectedDate.year, selectedDate.month, selectedDate.day);
        print(selectedDate);
      });
  }

  Widget buildEsterelizado() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Esterelizado',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
            alignment: Alignment.center,
            decoration: kBoxDecorationStyle,
            height: 60.0,
            child: DropdownButton(
              dropdownColor: Color(0xFF6CA8F1),
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              hint: Text(
                'Seleccione la opción',
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              value: esterilizado,
              onChanged: (newValue) {
                print(newValue);
                setState(() {
                  esterilizado = newValue;
                });
              },
              items: opcionesEster.map((generos) {
                return DropdownMenuItem(
                  child: new Text(generos),
                  value: generos,
                );
              }).toList(),
            )),
      ],
    );
  }

  Widget genero() {
    return Column(
      children: [
        Text(
          "Genero",
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              child: Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Stack(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Icon(
                        FontAwesomeIcons.paw,
                        color: Colors.blue,
                        size: 50,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        "Macho",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Stack(
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Icon(
                      FontAwesomeIcons.paw,
                      color: Colors.pink,
                      size: 50,
                    ),
                  ),
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        "Hembra",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      )),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
