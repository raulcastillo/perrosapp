import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:carnet_digital_mascotas/home/home.dart';
import 'package:carnet_digital_mascotas/login/login.dart';
import 'package:flutter/material.dart';

class SplashScreenPage extends StatefulWidget {
  SplashScreenPage({Key key, this.route});
  String route;
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splashIconSize: 500,
      splash: 'assets/chaconImagen.jpeg',
      nextScreen: widget.route == '/login' ? LoginScreen() : Home(),
      splashTransition: SplashTransition.scaleTransition,
    );
  }
}
