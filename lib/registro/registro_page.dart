import 'dart:convert';

import 'package:carnet_digital_mascotas/login/login.dart';
import 'package:carnet_digital_mascotas/models/colonias_model.dart';
import 'package:carnet_digital_mascotas/providers/colonias_provider.dart';
import 'package:carnet_digital_mascotas/providers/login_provider.dart';
import 'package:carnet_digital_mascotas/utilities/constants.dart';
import 'package:carnet_digital_mascotas/widgets/lavel_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:search_choices/search_choices.dart';
import 'package:toast/toast.dart';

Future<List<Colonia>> lista;

class Registro extends StatefulWidget {
  @override
  _RegistroState createState() => _RegistroState();
}

class _RegistroState extends State<Registro> {
  List<String> _generos = ['Hombre', 'Mujer'];
  String _selectedSexo;
  bool _rememberMe = false;
  DateTime selectedDate = DateTime(1900, 1);
  TextEditingController correo = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController apellidoP = TextEditingController();
  TextEditingController apellidoM = TextEditingController();
  TextEditingController username = TextEditingController();
  TextEditingController telefono = TextEditingController();
  TextEditingController interior = TextEditingController();
  TextEditingController exterior = TextEditingController();
  TextEditingController codigoP = TextEditingController();
  TextEditingController nombre = TextEditingController();
  TextEditingController calle = TextEditingController();
  TextEditingController localidad = TextEditingController();
  TextEditingController municipio = TextEditingController();
  int coloniaId;
  int municipioId;
  int localidadId;
  final formKey = GlobalKey<FormState>();

  Widget _buildEmailTF() {
    return LavelText(
        keyboardType: TextInputType.emailAddress,
        validar: true,
        prefixIcon: Icon(
          Icons.email,
          color: Colors.white,
        ),
        controller: correo,
        lavel: 'Correo electronico',
        hintText: 'Escribe tu correo electronico');
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Contraseña',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: password,
            obscureText: true,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Escribe tu contraseña',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildRegisterBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () {
          if (formKey.currentState.validate()) {
            BuildContext circular = context;
            showDialog(
                context: circular,
                builder: (BuildContext context) {
                  return Lottie.asset('assets/46864-lovely-cats (2).json');
                });

            registrar(
                    nombre.text,
                    apellidoP.text,
                    apellidoM.text,
                    selectedDate.toString(),
                    _selectedSexo,
                    password.text,
                    username.text,
                    telefono.text,
                    correo.text,
                    calle.text,
                    interior.text,
                    _currentColonia.id,
                    localidadId,
                    municipioId,
                    exterior.text)
                .then((value) {
              print(value.body);
              var a = jsonDecode(value.body);

              if (a['status'] == false) {
                Navigator.pop(circular);
                Toast.show('${a["msg"]}', context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
              } else {
                Navigator.popAndPushNamed(context, "/login");
                Toast.show('${a["msg"]}', context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
              }
            });
          }
        },
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.white,
        child: Text(
          'Registrar',
          style: TextStyle(
            color: Color(0xFF527DAA),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget _buildLogupBtn() {
    return GestureDetector(
      onTap: () {
        //aqui te debe de mandar a la otra pantalla
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      },
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: 'Ya tienes una cuenta? ',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w400,
              ),
            ),
            TextSpan(
              text: 'logueate',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 1),
        lastDate: DateTime(2021, 12));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        selectedDate =
            DateTime(selectedDate.year, selectedDate.month, selectedDate.day);
        print(selectedDate);
      });
  }

  @override
  void initState() {
    super.initState();
    // getColonias(77010).then((value) {
    //   print(value);
    // });
  }

  Colonia _currentColonia;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF73AEF5),
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFF73AEF5),
                      Color(0xFF61A4F1),
                      Color(0xFF478DE0),
                      Color(0xFF398AE5),
                    ],
                    stops: [0.1, 0.4, 0.7, 0.9],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 30.0,
                  ),
                  child: Form(
                    key: formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Registro",
                            style: GoogleFonts.pacifico(fontSize: 40.0),
                            textAlign: TextAlign.center),
                        SizedBox(height: 10.0),
                        _buildNombre(),
                        SizedBox(height: 10.0),
                        _buildApellidoP(),
                        SizedBox(height: 10.0),
                        _buildApellidoM(),
                        SizedBox(height: 10.0),
                        _getDatePickerEnabled(),
                        SizedBox(height: 10.0),
                        _buildSexo(),
                        SizedBox(height: 10.0),
                        _buildUsername(),
                        SizedBox(height: 10.0),
                        _buildEmailTF(),
                        SizedBox(height: 10.0),
                        _buildPasswordTF(),
                        SizedBox(height: 10.0),
                        _buildTelefono(),
                        SizedBox(height: 10.0),
                        _buildCalle(),
                        SizedBox(height: 10.0),
                        _buildNumeroE(),
                        SizedBox(height: 10.0),
                        _buildNumeroI(),
                        SizedBox(height: 10.0),
                        _buildCodigoP(),
                        SizedBox(height: 10.0),
                        _getColonia(),
                        SizedBox(height: 10.0),
                        _currentColonia != null
                            ? _getLocalidades()
                            : Container(),
                        SizedBox(height: 10.0),
                        _currentColonia != null ? _getMunicipio() : Container(),
                        SizedBox(height: 10.0),
                        _buildRegisterBtn(),
                        _buildLogupBtn(),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _getColonia() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Colonia',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 70.0,
          child: FutureBuilder<List<Colonia>>(
            future: lista,
            builder: (context, data) {
              if (data.connectionState == ConnectionState.done &&
                  data.data.isNotEmpty) {
                return Container(
                    child: SearchChoices.single(
                  iconDisabledColor: Colors.white,
                  iconEnabledColor: Colors.white,
                  menuBackgroundColor: Colors.white,
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 18),
                  // validator: (value){
                  //        if(value == null && banderaVColonia == false){
                  //          return 'Por favor elija su colonia';
                  //        }
                  // },
                  closeButton: 'Cerrar',
                  isExpanded: true,
                  underline: SizedBox(),
                  value: _currentColonia,
                  items: data.data
                      .map((comunidad) => DropdownMenuItem<Colonia>(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  comunidad.descripcion,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  // style: TextStyle(
                                  //   fontSize: 15,
                                  //   // color: Colors.black,
                                  //   fontFamily: 'OpenSans',
                                  // ),
                                ),
                              ],
                            ),
                            value: comunidad,
                          ))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      if (value != null) {
                        _currentColonia = value;
                        //banderaVColonia == true;
                        getLocalidades(int.parse(codigoP.text)).then((value) {
                          if (value != null) {
                            var a = json.decode(value.body);
                            if (a['data'] != null) {
                              print(a['data']);
                              if (a['data']['localidad'] != null) {
                                localidad.text =
                                    a['data']['localidad']['descripcion'];
                                localidadId = a['data']['localidad']['id'];
                              }
                              if (a['data']['municipio'] != null) {
                                municipio.text =
                                    a['data']['municipio']['descripcion'];
                                municipioId = a['data']['municipio']['id'];
                              }
                            }
                          } else {
                            Toast.show(
                                'No se encontraron datos con ese codigo postal',
                                context,
                                duration: Toast.LENGTH_LONG,
                                gravity: Toast.CENTER);
                          }
                        });
                      } else {
                        setState(() {
                          _currentColonia = null;
                          // validadrLocalidad = true;
                        });
                      }
                    });
                  },
                  hint: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Colonia',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ],
                  ),
                ));
              }
              return Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _getLocalidades() {
    return LavelText(
        enable: false,
        prefixIcon: Icon(
          Icons.directions,
          color: Colors.white,
        ),
        controller: localidad,
        lavel: 'Localidades',
        hintText: "Localidades");
  }

  Widget _getMunicipio() {
    return LavelText(
        enable: false,
        prefixIcon: Icon(
          Icons.directions,
          color: Colors.white,
        ),
        controller: municipio,
        lavel: 'Municipio',
        hintText: "Municipio");
  }

  Widget _buildNombre() {
    return LavelText(
      validar: true,
      controller: nombre,
      lavel: 'Nombre',
      hintText: 'Escribe tu nombre',
      prefixIcon: Icon(
        Icons.person,
        color: Colors.white,
      ),
    );
  }

  Widget _buildApellidoP() {
    return LavelText(
      validar: true,
      controller: apellidoP,
      lavel: 'Apellido paterno',
      hintText: 'Escribe tu apellido paterno',
      prefixIcon: Icon(
        Icons.person,
        color: Colors.white,
      ),
    );
  }

  Widget _buildApellidoM() {
    return LavelText(
      controller: apellidoM,
      lavel: 'Apellido materno',
      hintText: 'Escribe tu apellido materno',
      prefixIcon: Icon(
        Icons.person,
        color: Colors.white,
      ),
    );
  }

  Widget _buildUsername() {
    return LavelText(
      validar: true,
      controller: username,
      lavel: 'Nombre de usuario',
      hintText: 'Escribe tu nombre de usuario',
      prefixIcon: Icon(
        Icons.person,
        color: Colors.white,
      ),
    );
  }

  Widget _buildTelefono() {
    return LavelText(
      validar: true,
      controller: telefono,
      lavel: 'Telefono',
      hintText: 'Escribe tu telefono',
      prefixIcon: Icon(
        Icons.phone,
        color: Colors.white,
      ),
      keyboardType: TextInputType.phone,
    );
  }

  Widget _buildCalle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        LavelText(
            validar: true,
            prefixIcon: Icon(
              Icons.directions,
              color: Colors.white,
            ),
            controller: calle,
            lavel: 'Calle',
            hintText: 'Escribe tu calle'),
      ],
    );
  }

  Widget _buildNumeroI() {
    return LavelText(
      controller: interior,
      lavel: 'Numero Interior (opcional)',
      hintText: 'Escribe tu numero interior',
      prefixIcon: Icon(
        Icons.home,
        color: Colors.white,
      ),
    );
  }

  Widget _buildNumeroE() {
    return LavelText(
      validar: true,
      controller: exterior,
      lavel: 'Numero exterior',
      hintText: 'Escribe tu numero exterior',
      prefixIcon: Icon(
        Icons.home,
        color: Colors.white,
      ),
    );
  }

  Widget _buildCodigoP() {
    return LavelText(
        onChanged: (String codiPos) {
          if (codiPos.length == 5) {
            setState(() {
              lista = null;
              lista = getColonias(int.parse(codiPos));
              lista.then((value) {
                if (value.isEmpty) {
                  Toast.show('No se encuentra el código postal', context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                }
              });
            });
          }
          if (codiPos.length == 0) {
            setState(() {
              lista = null;
            });
          }
        },
        validar: true,
        keyboardType: TextInputType.number,
        prefixIcon: Icon(
          Icons.post_add_outlined,
          color: Colors.white,
        ),
        controller: codigoP,
        lavel: 'Codigo Postal',
        hintText: 'Escribe tu codigo postal');
  }

  Widget _getDatePickerEnabled() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Fecha de nacimiento',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: Padding(
            padding: EdgeInsets.only(left: 40),
            child: InkWell(
              onTap: () {
                _selectDate(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  selectedDate == DateTime(1900, 1)
                      ? Text(
                          "Seleccione su fecha",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                          //style: CustomTheme.of(context).textTheme.subhead.copyWith(),
                        )
                      : Text(
                          "${selectedDate.day} - ${selectedDate.month} - ${selectedDate.year}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                          //style: CustomTheme.of(context).textTheme.subhead.copyWith(),
                        ),
                  Icon(Icons.arrow_drop_down,
                      color: Theme.of(context).brightness == Brightness.light
                          ? Colors.grey.shade700
                          : Colors.white70),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSexo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Genero',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
            alignment: Alignment.center,
            decoration: kBoxDecorationStyle,
            height: 60.0,
            child: DropdownButton(
              dropdownColor: Color(0xFF6CA8F1),
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              hint: Text(
                'Seleccione su genero',
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              value: _selectedSexo,
              onChanged: (newValue) {
                print(newValue);
                setState(() {
                  _selectedSexo = newValue;
                });
              },
              items: _generos.map((generos) {
                return DropdownMenuItem(
                  child: new Text(generos),
                  value: generos,
                );
              }).toList(),
            )),
      ],
    );
  }
}
