import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() => runApp(MenuWidget());

class MenuWidget extends StatefulWidget {
  MenuWidget({Key key}) : super(key: key);
  _MenuWidgetState createState() => _MenuWidgetState();
}

class _MenuWidgetState extends State<MenuWidget> {
  // final temaController = Get.put(TemaProvider());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    double tamano = 50;
    return Container(
      color: Colors.lightBlue,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: size.height * .35,
              child: DrawerHeader(
                  // decoration: BoxDecoration(
                  //     border: Border.all(
                  //         color: Color.fromRGBO(248, 244, 235, 1))),
                  child: Center(
                child: ListTile(
                  title: Text(
                      //  prefs.nombre,
                      'Abdiel Aziel Canul Chavez'),
                  // subtitle: Text(
                  //     //prefs.userName,
                  //     'Abdiel Canul'),
                  leading: CircleAvatar(
                    backgroundColor: Color.fromRGBO(172, 39, 47, 1),
                    backgroundImage: NetworkImage(
                        'https://www.yourtrainingedge.com/wp-content/uploads/2019/05/background-calm-clouds-747964.jpg'),
                    minRadius: 20,
                    maxRadius: 25,
                  ),
                  onTap: () {
                    //aqui se manda a los datos del usuario
                    Navigator.popAndPushNamed(context, '/userPage');
                  },
                ),
              )),
            ),
            Container(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: tamano,
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        leading: Icon(
                          FontAwesomeIcons.home,
                          size: 25,
                          color: Colors.black,
                        ),
                        title: Container(
                          child: Text("Inicio"),
                        ),
                        onTap: () {
                          // if (banderaInicio == true) {
                          //   Navigator.pop(context);
                          // } else if (banderaInicio == false) {
                          Navigator.pushReplacementNamed(context, '/home');
                          // }
                        },
                      ),
                    ),
                    // prefs.registroContactos == true
                    //     ?
                    Container(
                      height: tamano,
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        leading: Icon(
                          FontAwesomeIcons.search,
                          size: 25,
                          color: Colors.black,
                        ),
                        title: Container(
                          child: Text("Mascotas Perdidas"),
                        ),
                        onTap: () {
                          Navigator.pushReplacementNamed(
                              context, '/mascotasPerdidas');
                          // Navigator.pop(context);
                        },
                      ),
                    ),
                    Container(
                      height: tamano,
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        leading: Icon(
                          FontAwesomeIcons.store,
                          size: 25,
                          color: Colors.black,
                        ),
                        title: Container(
                          child: Text("Negocios"),
                        ),
                        onTap: () {
                          Navigator.pushReplacementNamed(context, '/negocios');
                          // Navigator.pop(context);
                        },
                      ),
                    ),
                    // : Container(),
                    // prefs.contactos == true
                    //     ?
                    // Container(
                    //   height: tamano,
                    //   child: ListTile(
                    //     contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    //     leading: Icon(
                    //       Icons.contacts,
                    //     ),
                    //     title: Container(
                    //       child: Text("Mis mascotas"),
                    //     ),
                    //     onTap: () {
                    //       Navigator.pushReplacementNamed(
                    //           context, '/mismascotas');
                    //     },
                    //   ),
                    // ),
                    Container(
                      height: tamano,
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        leading: Icon(
                          FontAwesomeIcons.paw,
                          size: 25,
                          color: Colors.black,
                        ),
                        title: Container(
                          child: Text("Mis mascotas"),
                        ),
                        onTap: () {
                          Navigator.pushReplacementNamed(
                              context, '/mismascotas2');
                        },
                      ),
                    ),
                    // : Container(),
                    // prefs.registrar
                    //     ?
                    Container(
                      height: tamano,
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        leading: Image(
                          height: 30,
                          image: AssetImage(
                            'assets/adopcion.png',
                          ),
                          color: Colors.black,
                        ),
                        title: Container(
                          child: Text("Adopcion"),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.pushReplacementNamed(
                              context, '/registroForm');
                        },
                      ),
                    ),
                    // : Container(),
                    // prefs.problematicas
                    // ?
                    Container(
                      height: tamano,
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        leading: Icon(
                          FontAwesomeIcons.exclamationCircle,
                          size: 25,
                          color: Colors.black,
                        ),
                        title: Container(
                          child: Text("Configuración"),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.pushReplacementNamed(
                              context, '/problematica');
                        },
                      ),
                    ),
                    //: Container(),
                    // prefs.semblanza
                    //     ?
                    Container(
                      height: tamano,
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        leading: Icon(
                          FontAwesomeIcons.signOutAlt,
                          size: 25,
                          color: Colors.black,
                        ),
                        title: Container(
                          child: Text("Cerrar sesión"),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          //prefs.cle
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              '/login', (route) => false);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// Widget buildDrawer(){
//     return Container(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//             ListTile(
//               title: Text('Inicio'),
//               onTap: () {
//                 // Update the state of the app.
//                 // ...
//               },
//             ),
//             ListTile(
//               title: Text('Mascotas perdidas'),
//               onTap: () {
//                 // Update the state of the app.
//                 // ...
//               },
//             ),
//             ListTile(
//               title: Text('Mis mascotas'),
//               onTap: () {
//                 // Update the state of the app.
//                 // ...
//               },
//             ),
//             ListTile(
//               title: Text('Adopcion'),
//               onTap: () {
//                 // Update the state of the app.
//                 // ...
//               },
//             ),
//             ListTile(
//               title: Text('Configuracion'),
//               onTap: () {
//                 // Update the state of the app.
//                 // ...
//               },
//             ),
//             ListTile(
//               title: Text('Cerrar sesion'),
//               onTap: () {
//                 // Update the state of the app.
//                 // ...
//               },
//             ),
//         ],
//       ),
//     );
// }
