import 'package:flutter/material.dart';

class Search extends StatefulWidget {
  Search({Key key, this.size, this.hintText, this.onChanged, this.padding});
  final size;
  final Padding padding;
  final String hintText;
  final ValueChanged<String> onChanged;
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      autocorrect: true,
      decoration: InputDecoration(
        filled: true,
        fillColor: Color.fromRGBO(255, 255, 255, 1),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: Colors.black26)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: Colors.black26)),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(100),
          borderSide: BorderSide(color: Colors.black26),
        ),
        contentPadding: EdgeInsets.fromLTRB(12, 0, 12, 0),
        hintText: widget.hintText,
        hintStyle: TextStyle(
          fontWeight: FontWeight.w600,
          color: Color.fromRGBO(152, 160, 156, 1),
          fontSize: 16,
        ),
        prefixIcon: IconButton(
          icon: Icon(
            Icons.search,
            color: Color.fromRGBO(122, 130, 126, 1),
          ),
          onPressed: () {},
          iconSize: 30.0,
        ),
      ),
      onChanged: widget.onChanged,
    );
  }
}
