import 'package:drawer_swipe/drawer_swipe.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppbarBalchi extends StatefulWidget {
  AppbarBalchi({Key key, this.label, this.drawerKey2});
  final String label;
  final drawerKey2;
  @override
  _AppbarBalchiState createState() => _AppbarBalchiState();
}

class _AppbarBalchiState extends State<AppbarBalchi> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Center(
        child: Text(
          widget.label,
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      leading: InkWell(
          onTap: () {
            if (widget.drawerKey2.currentState.isOpened()) {
              widget.drawerKey2.currentState.closeDrawer();
            } else {
              widget.drawerKey2.currentState.openDrawer();
            }
          },
          child: Icon(
            Icons.menu,
            color: Colors.black,
          )),
      shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20))),
      actions: [
        IconButton(
          icon: Icon(
            Icons.person_outline_rounded,
            color: Colors.black,
          ),
          onPressed: () {
            // do something
          },
        )
      ],
    );
  }
}

class AppbarBalchi2 extends StatefulWidget {
  AppbarBalchi2({Key key, this.label, this.drawerKey2});
  final String label;
  final drawerKey2;
  @override
  _AppbarBalchiState2 createState() => _AppbarBalchiState2();
}

class _AppbarBalchiState2 extends State<AppbarBalchi2> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Center(
        child: Text(
          widget.label,
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      leading: IconButton(
        onPressed: () {
          if (widget.drawerKey2.currentState.isOpened()) {
            widget.drawerKey2.currentState.closeDrawer();
          } else {
            widget.drawerKey2.currentState.openDrawer();
          }
        },
        icon: SvgPicture.asset(
          "assets/menu2.svg",
          width: 15,
          color: Colors.black,
          height: 15,
          fit: BoxFit.cover,
        ),
      ),
      // InkWell(
      //   onTap: () {

      //   },
      //   child: Icon(
      //     Icons.menu,
      //     color: Colors.black,
      //   ),
      // ),
      shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20))),
      actions: [
        IconButton(
          icon: Icon(
            Icons.person_outline_rounded,
            color: Colors.black,
          ),
          onPressed: () {
            // do something
          },
        )
      ],
    );
  }
}
