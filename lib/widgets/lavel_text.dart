import 'package:carnet_digital_mascotas/utilities/constants.dart';
import 'package:flutter/material.dart';

class LavelText extends StatelessWidget {
  LavelText(
      {Key key,
      @required this.controller,
      @required this.lavel,
      @required this.hintText,
      this.maxLeng,
      this.keyboardType,
      this.onChanged,
      this.validar,
      this.suffixIcon,
      this.mostrarContrasena,
      this.prefixIcon,
      this.validator,
      this.enable});
  final TextEditingController controller;
  final String lavel;
  final String hintText;
  final int maxLeng;
  final String Function(String) validator;

  ///por defecto sera de tipo TextInputType.text,
  final TextInputType keyboardType;
  final Function(String) onChanged;
  final Widget suffixIcon;
  final bool mostrarContrasena;
  final Widget prefixIcon;

  ///por defecto sera falso
  final bool validar;

  ///por defecto es true
  final bool enable;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          '${this.lavel}',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextFormField(
            enabled: enable == null ? true : enable,
            validator: validar == true
                ? (vali) {
                    if (vali.isEmpty) {
                      return 'El campo $lavel es obligatorio';
                    } else if (maxLeng != null) {
                      if (vali.length < maxLeng) {
                        return 'El campo $lavel debe ser de $maxLeng digitos';
                      }
                    }
                    return null;
                  }
                : null,
            onChanged: this.onChanged,
            // maxLength: this.maxLeng,
            controller: this.controller,
            keyboardType:
                keyboardType == null ? TextInputType.text : keyboardType,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            obscureText: mostrarContrasena == null ? false : mostrarContrasena,
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: prefixIcon,
                hintText: '$hintText',
                hintStyle: kHintTextStyle,
                suffixIcon: this.suffixIcon),
          ),
        ),
      ],
    );
  }
}
