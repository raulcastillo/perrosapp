import 'package:carnet_digital_mascotas/Adopcion/AdopcionPage.dart';
import 'package:carnet_digital_mascotas/home/home.dart';
import 'package:carnet_digital_mascotas/introduccion/introduccion_page.dart';
import 'package:carnet_digital_mascotas/mascotas_perdidas/detalles_mascota_perdida.dart';
import 'package:carnet_digital_mascotas/mascotas_perdidas/mascotas_perdidad_page.dart';
import 'package:carnet_digital_mascotas/misMascotas/mis_mascotas2.dart';
import 'package:carnet_digital_mascotas/misMascotas/mis_mascotas_page.dart';
import 'package:carnet_digital_mascotas/negocios/negocios.dart';
import 'package:carnet_digital_mascotas/page/addMascotas/agregar_Mascota.dart';
import 'package:carnet_digital_mascotas/recursos/variables.dart';
import 'package:carnet_digital_mascotas/splashScreen/splashscreenPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';

import 'login/login.dart';

Future<void> main() async {
  await prefs.initPrefs();
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    String route = '/login';

    print(prefs.token.toString());

    if (prefs.token.toString() != "") {
      route = '/home';
    } else {
      route = '/login';
    }

    return GetMaterialApp(
      locale: Locale('es', 'ES'),
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale("es", "ES"),
        const Locale('en', 'US'),
      ],
      debugShowCheckedModeBanner: false,
      title: 'Balchi',
      getPages: [
        GetPage(
          name: '/',
          page: () => SplashScreenPage(
            route: route,
          ),
        ),
        GetPage(name: '/login', page: () => LoginScreen()),
        GetPage(name: '/home', page: () => Home()),
        GetPage(name: "/mascotasPerdidas", page: () => MascotasPerdidasPage()),
        GetPage(name: "/adopciones", page: () => AdopcionPage()),
        GetPage(name: '/negocios', page: () => NegociosPage()),
        GetPage(name: '/mismascotas', page: () => MisMascotasPage()),
        GetPage(name: '/mismascotas2', page: () => MisMascotasPage2()),
        GetPage(name: "/introduccion", page: () => Introduccion()),
        GetPage(name: "/agregarMascota", page: () => AgregarMascota()),
        GetPage(
            name: '/detalleMascotaPerdida',
            page: () => DetalleMascotasPerdidas()),
      ],
    );
  }
}
