import 'package:carnet_digital_mascotas/widgets/appbarBalchi.dart';
import 'package:carnet_digital_mascotas/widgets/menu.dart';
import 'package:drawer_swipe/drawer_swipe.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MisMascotasPage2 extends StatefulWidget {
  @override
  _MisMascotasPage2State createState() => _MisMascotasPage2State();
}

class _MisMascotasPage2State extends State<MisMascotasPage2> {
  var drawerKey2 = GlobalKey<SwipeDrawerState>();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        child: SwipeDrawer(
          radius: 20,
          key: drawerKey2,
          hasClone: false,
          bodyBackgroundPeekSize: 30,
          backgroundColor: Colors.lightBlue,
          drawer: MenuWidget(),
          child: buildBody(size),
        ),
      ),
    );
  }

  Widget buildBody(final size) {
    return Column(
      children: [
        AppbarBalchi2(
          label: "Balchi",
          drawerKey2: drawerKey2,
        ),
        Container(
          height: size.height * .90,
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 2,
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 8.0,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/agregarMascota");
                },
                child: Container(
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black45,
                        blurRadius: 15.0,
                        spreadRadius: 0.0,
                        offset: Offset(0.0, 0.0),
                      ),
                    ],
                  ),
                  child: Center(
                      child: Icon(
                    FontAwesomeIcons.plus,
                    size: 50,
                    color: Colors.lightBlue,
                  )),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
