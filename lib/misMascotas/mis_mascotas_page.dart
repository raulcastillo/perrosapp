import 'package:carnet_digital_mascotas/widgets/menu.dart';
import 'package:drawer_swipe/drawer_swipe.dart';
import 'package:flutter/material.dart';

class MisMascotasPage extends StatefulWidget {
  @override
  _MisMascotasPageState createState() => _MisMascotasPageState();
}

class _MisMascotasPageState extends State<MisMascotasPage> {
  var drawerKey2 = GlobalKey<SwipeDrawerState>();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      // appBar: AppBar(
      //   title: Center(
      //       child: Text(
      //     'Mis mascotas',
      //     style: TextStyle(color: Colors.black),
      //   )),
      //   backgroundColor: Colors.white,
      //   leading: InkWell(
      //       onTap: () {
      //         if (drawerKey2.currentState.isOpened()) {
      //           drawerKey2.currentState.closeDrawer();
      //         } else {
      //           drawerKey2.currentState.openDrawer();
      //         }
      //       },
      //       child: Icon(
      //         Icons.menu,
      //         color: Colors.black,
      //       )),
      //   shape: RoundedRectangleBorder(
      //       borderRadius: new BorderRadius.only(
      //           bottomLeft: Radius.circular(20),
      //           bottomRight: Radius.circular(20))),
      //   actions: [
      //     IconButton(
      //       icon: Icon(
      //         Icons.person_outline_rounded,
      //         color: Colors.black,
      //       ),
      //       onPressed: () {
      //         // do something
      //       },
      //     )
      //   ],
      // ),
      body: SingleChildScrollView(
        child: SwipeDrawer(
          radius: 20,
          key: drawerKey2,
          hasClone: false,
          bodyBackgroundPeekSize: 30,
          backgroundColor: Colors.lightBlue,
          drawer: MenuWidget(),
          child: buildBody(size),
        ),
      ),
    );
  }

  Widget buildBody(final size) {
    return Column(
      children: [
        AppBar(
          title: Center(
              child: Text(
            'Balchi',
            style: TextStyle(color: Colors.black),
          )),
          backgroundColor: Colors.white,
          leading: InkWell(
              onTap: () {
                if (drawerKey2.currentState.isOpened()) {
                  drawerKey2.currentState.closeDrawer();
                } else {
                  drawerKey2.currentState.openDrawer();
                }
              },
              child: Icon(
                Icons.menu,
                color: Colors.black,
              )),
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20))),
          actions: [
            IconButton(
              icon: Icon(
                Icons.person_outline_rounded,
                color: Colors.black,
              ),
              onPressed: () {
                // do something
              },
            )
          ],
        ),
        Container(
          height: size.height * .90,
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 2,
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 8.0,
            children: [
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 20.0,
                      spreadRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
                    SizedBox(height: 20),
                    Text(
                      "text",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget buildBody2(final size) {
    return CustomScrollView(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      slivers: <Widget>[
        SliverToBoxAdapter(
          child: Container(
            // height: 136,
            //margin: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AppBar(
                  title: Center(
                    child: Text(
                      'My Pet\'s',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                  backgroundColor: Colors.white,
                  leading: InkWell(
                      onTap: () {
                        if (drawerKey2.currentState.isOpened()) {
                          drawerKey2.currentState.closeDrawer();
                        } else {
                          drawerKey2.currentState.openDrawer();
                        }
                      },
                      child: Icon(
                        Icons.menu,
                        color: Colors.black,
                      )),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  actions: [
                    IconButton(
                      icon: Icon(
                        Icons.person_outline_rounded,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        // do something
                      },
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        SliverGrid.count(
          crossAxisCount: 2,
          children: <Widget>[
            card(),
            card(),
            card(),
            card(),
          ],
        ),
      ],
    );
  }

  Widget card() {
    return GestureDetector(
      onTap: () {
        //Navigator.of(context).pushNamed(MyPets.routeName);
      },
      child: Container(
        margin: EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black45,
              blurRadius: 20.0,
              spreadRadius: 0.0,
              offset: Offset(0.0, 0.0),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //imageType == 'svg' ? SvgPicture.asset('assets/images/$image.svg') : Image.asset('assets/images/$image.png'),
            SizedBox(height: 20),
            Text(
              "text",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }
}
